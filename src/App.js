import './App.css';

import React, { Component } from 'react';

import TodoForm from './components/TodoForm';
import logo from './logo.svg';
import { todos } from './todos.json';

// import Navigation from './components/Navigation';

class App extends Component {
  constructor() {
    super();

    this.state = {
      todos
    }
  };

  render() {
    const tasks = this.state.todos.map((todo, i) => {
      return (
        <div key={i} className="col-md-4">
          <div className="card mt-4">
            <div className="card-header">
              <h3>
                {todo.title}
              </h3>
              <span className="badge badge-pill badge-danger ml-2">
                {todo.priority}
              </span>
            </div>
            <div className="card-body"><p>{todo.description}</p>
              <p><mark>{todo.responsible}</mark></p></div>
          </div>
        </div>
      );
    })

    return (
      <div className="App" >
        <nav className="navbar navbar-dark bg-dark">
          <a href="" className="text-white">Tasks <span className="badge badge-pill badge-light ml-2">
            {tasks.length}
          </span></a>

        </nav>

        <div className="container">
          <div className="row mt-4">
            <TodoForm />
            {tasks}
          </div>
        </div>
        <img src={logo} className="App-logo" alt="logo" />
      </div>
    );
  }
}

export default App;
