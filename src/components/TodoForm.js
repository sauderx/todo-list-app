import React, { Component } from 'react';

class TodoForm extends Component {

    handleInput = (e) => {
        console.log(e.target.value, " | ", e.target.name);
    }

    render() {
        return (
            <div className="card mt-4">
                <form className="card-body">
                    <div className="form-group">
                        <input type="text" name="title" className="form-control" placeholder="title" onChange={this.handleInput}></input>
                    </div>
                    <div className="form-group">
                        <input type="text" name="responsible" className="form-control" placeholder="responsible" onChange={this.handleInput}></input>
                    </div>
                    <div className="form-group">
                        <input type="text" name="description" className="form-control" placeholder="description" onChange={this.handleInput}></input>
                    </div>
                    <div className="form-group">
                        <select name="priority" className="form-control" onChange={this.handleInput}>
                            <option>low</option>
                            <option>medium</option>
                            <option>high</option>
                        </select>
                    </div>
                </form>
            </div>
        )
    }
}

export default TodoForm;